---
title: 民法典
comments: false
---
2020年5月28日，十三届全国人大三次会议表决通过了《中华人民共和国民法典》，自2021年1月1日起施行。婚姻法、继承法、民法通则、收养法、担保法、合同法、物权法、侵权责任法、民法总则同时废止。  


《中华人民共和国民法典》共7编、1260条，各编依次为[总则](/laws/minfadian/zong)、[物权](/laws/minfadian/zong)、[合同](/laws/minfadian/zong)、[人格权](/laws/minfadian/ren)、[婚姻家庭](/laws/minfadian/hun)、[继承](/laws/minfadian/hun)、[侵权责任](/laws/minfadian/qin)，以及[附则](/laws/minfadian/qin)。  


